sj_sheet.png
format: RGBA8888
filter: Linear,Linear
repeat: none
battle_stance
  rotate: false
  xy: 142, 2
  size: 37, 51
  orig: 37, 51
  offset: 0, 0
  index: 1
battle_stance
  rotate: false
  xy: 170, 55
  size: 37, 51
  orig: 37, 51
  offset: 0, 0
  index: 2
battle_stance
  rotate: false
  xy: 181, 2
  size: 37, 51
  orig: 37, 51
  offset: 0, 0
  index: 3
battle_stance_crouch
  rotate: false
  xy: 988, 45
  size: 39, 38
  orig: 39, 38
  offset: 0, 0
  index: 1
battle_stance_crouch
  rotate: false
  xy: 1206, 2
  size: 40, 30
  orig: 40, 30
  offset: 0, 0
  index: 2
battle_stance_crouch
  rotate: false
  xy: 828, 95
  size: 49, 31
  orig: 49, 31
  offset: 0, 0
  index: 3
battle_stance_crouch
  rotate: false
  xy: 1097, 71
  size: 49, 31
  orig: 49, 31
  offset: 0, 0
  index: 4
battle_stance_crouch
  rotate: false
  xy: 1115, 36
  size: 48, 31
  orig: 48, 31
  offset: 0, 0
  index: 5
battle_stance_walk
  rotate: false
  xy: 209, 55
  size: 35, 51
  orig: 35, 51
  offset: 0, 0
  index: 1
battle_stance_walk
  rotate: false
  xy: 432, 2
  size: 35, 50
  orig: 35, 50
  offset: 0, 0
  index: 2
battle_stance_walk
  rotate: false
  xy: 220, 2
  size: 35, 51
  orig: 35, 51
  offset: 0, 0
  index: 3
battle_stance_walk
  rotate: false
  xy: 104, 59
  size: 35, 52
  orig: 35, 52
  offset: 0, 0
  index: 4
battle_stance_walk
  rotate: false
  xy: 246, 55
  size: 35, 51
  orig: 35, 51
  offset: 0, 0
  index: 5
battle_stance_walk
  rotate: false
  xy: 469, 2
  size: 35, 50
  orig: 35, 50
  offset: 0, 0
  index: 6
battle_stance_walk
  rotate: false
  xy: 257, 2
  size: 35, 51
  orig: 35, 51
  offset: 0, 0
  index: 7
battle_stance_walk
  rotate: false
  xy: 105, 2
  size: 35, 52
  orig: 35, 52
  offset: 0, 0
  index: 8
crouch
  rotate: false
  xy: 1022, 85
  size: 33, 38
  orig: 33, 38
  offset: 0, 0
  index: 1
crouch
  rotate: false
  xy: 1148, 69
  size: 35, 31
  orig: 35, 31
  offset: 0, 0
  index: 2
crouch
  rotate: false
  xy: 1029, 45
  size: 44, 35
  orig: 44, 35
  offset: 0, 0
  index: 3
crouch_roll
  rotate: false
  xy: 69, 2
  size: 34, 55
  orig: 34, 55
  offset: 0, 0
  index: 1
crouch_roll
  rotate: false
  xy: 616, 101
  size: 30, 24
  orig: 30, 24
  offset: 0, 0
  index: 10
crouch_roll
  rotate: false
  xy: 1057, 82
  size: 38, 32
  orig: 38, 32
  offset: 0, 0
  index: 11
crouch_roll
  rotate: false
  xy: 1164, 2
  size: 40, 31
  orig: 40, 31
  offset: 0, 0
  index: 2
crouch_roll
  rotate: false
  xy: 748, 98
  size: 38, 28
  orig: 38, 28
  offset: 0, 0
  index: 3
crouch_roll
  rotate: false
  xy: 1222, 67
  size: 37, 27
  orig: 37, 27
  offset: 0, 0
  index: 4
crouch_roll
  rotate: false
  xy: 680, 100
  size: 28, 26
  orig: 28, 26
  offset: 0, 0
  index: 5
crouch_roll
  rotate: false
  xy: 648, 101
  size: 30, 24
  orig: 30, 24
  offset: 0, 0
  index: 6
crouch_roll
  rotate: false
  xy: 788, 98
  size: 38, 28
  orig: 38, 28
  offset: 0, 0
  index: 7
crouch_roll
  rotate: false
  xy: 1222, 96
  size: 37, 27
  orig: 37, 27
  offset: 0, 0
  index: 8
crouch_roll
  rotate: false
  xy: 710, 100
  size: 28, 26
  orig: 28, 26
  offset: 0, 0
  index: 9
crouch_slash
  rotate: false
  xy: 880, 2
  size: 46, 43
  orig: 46, 43
  offset: 0, 0
  index: 1
crouch_slash
  rotate: false
  xy: 1034, 2
  size: 45, 33
  orig: 45, 33
  offset: 0, 0
  index: 10
crouch_slash
  rotate: false
  xy: 1206, 34
  size: 39, 31
  orig: 39, 31
  offset: 0, 0
  index: 11
crouch_slash
  rotate: false
  xy: 62, 60
  size: 40, 56
  orig: 40, 56
  offset: 0, 0
  index: 2
crouch_slash
  rotate: false
  xy: 730, 2
  size: 48, 46
  orig: 48, 46
  offset: 0, 0
  index: 3
crouch_slash
  rotate: false
  xy: 426, 55
  size: 51, 50
  orig: 51, 50
  offset: 0, 0
  index: 4
crouch_slash
  rotate: false
  xy: 933, 46
  size: 53, 41
  orig: 53, 41
  offset: 0, 0
  index: 5
crouch_slash
  rotate: false
  xy: 2, 2
  size: 65, 56
  orig: 65, 56
  offset: 0, 0
  index: 6
crouch_slash
  rotate: false
  xy: 2, 60
  size: 58, 56
  orig: 58, 56
  offset: 0, 0
  index: 7
crouch_slash
  rotate: false
  xy: 959, 89
  size: 61, 36
  orig: 61, 36
  offset: 0, 0
  index: 8
crouch_slash
  rotate: false
  xy: 921, 92
  size: 36, 34
  orig: 36, 34
  offset: 0, 0
  index: 9
crouch_walk
  rotate: false
  xy: 1081, 2
  size: 37, 32
  orig: 37, 32
  offset: 0, 0
  index: 1
crouch_walk
  rotate: false
  xy: 1185, 67
  size: 35, 31
  orig: 35, 31
  offset: 0, 0
  index: 2
crouch_walk
  rotate: false
  xy: 1165, 35
  size: 38, 30
  orig: 38, 30
  offset: 0, 0
  index: 3
crouch_walk
  rotate: false
  xy: 1120, 2
  size: 42, 31
  orig: 42, 31
  offset: 0, 0
  index: 4
crouch_walk
  rotate: false
  xy: 879, 94
  size: 40, 32
  orig: 40, 32
  offset: 0, 0
  index: 5
crouch_walk
  rotate: false
  xy: 1075, 37
  size: 38, 32
  orig: 38, 32
  offset: 0, 0
  index: 6
looking_up
  rotate: false
  xy: 567, 2
  size: 24, 50
  orig: 24, 50
  offset: 0, 0
  index: 1
looking_up
  rotate: false
  xy: 356, 2
  size: 24, 51
  orig: 24, 51
  offset: 0, 0
  index: 2
run
  rotate: false
  xy: 795, 49
  size: 48, 44
  orig: 48, 44
  offset: 0, 0
  index: 1
run
  rotate: false
  xy: 715, 51
  size: 31, 47
  orig: 31, 47
  offset: 0, 0
  index: 10
run
  rotate: false
  xy: 980, 2
  size: 52, 41
  orig: 52, 41
  offset: 0, 0
  index: 11
run
  rotate: false
  xy: 831, 2
  size: 47, 44
  orig: 47, 44
  offset: 0, 0
  index: 2
run
  rotate: false
  xy: 845, 48
  size: 43, 44
  orig: 43, 44
  offset: 0, 0
  index: 3
run
  rotate: false
  xy: 696, 2
  size: 32, 47
  orig: 32, 47
  offset: 0, 0
  index: 4
run
  rotate: false
  xy: 571, 54
  size: 43, 49
  orig: 43, 49
  offset: 0, 0
  index: 5
run
  rotate: false
  xy: 928, 2
  size: 50, 42
  orig: 50, 42
  offset: 0, 0
  index: 6
run
  rotate: false
  xy: 780, 2
  size: 49, 45
  orig: 49, 45
  offset: 0, 0
  index: 7
run
  rotate: false
  xy: 748, 50
  size: 45, 46
  orig: 45, 46
  offset: 0, 0
  index: 8
run
  rotate: false
  xy: 890, 47
  size: 41, 43
  orig: 41, 43
  offset: 0, 0
  index: 9
slide_stop
  rotate: false
  xy: 647, 2
  size: 47, 47
  orig: 47, 47
  offset: 0, 0
  index: 1
slide_stop
  rotate: false
  xy: 666, 51
  size: 47, 47
  orig: 47, 47
  offset: 0, 0
  index: 2
slide_stop
  rotate: false
  xy: 616, 52
  size: 48, 47
  orig: 48, 47
  offset: 0, 0
  index: 3
slide_stop
  rotate: false
  xy: 593, 2
  size: 52, 48
  orig: 52, 48
  offset: 0, 0
  index: 4
stay
  rotate: false
  xy: 382, 2
  size: 23, 51
  orig: 23, 51
  offset: 0, 0
  index: 1
stay
  rotate: false
  xy: 375, 55
  size: 24, 51
  orig: 24, 51
  offset: 0, 0
  index: 2
stay
  rotate: false
  xy: 401, 55
  size: 23, 51
  orig: 23, 51
  offset: 0, 0
  index: 3
stay
  rotate: false
  xy: 407, 2
  size: 23, 51
  orig: 23, 51
  offset: 0, 0
  index: 4
turning
  rotate: false
  xy: 479, 54
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: -1
walk
  rotate: false
  xy: 283, 55
  size: 30, 51
  orig: 30, 51
  offset: 0, 0
  index: 1
walk
  rotate: false
  xy: 294, 2
  size: 30, 51
  orig: 30, 51
  offset: 0, 0
  index: 10
walk
  rotate: false
  xy: 506, 2
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: 2
walk
  rotate: false
  xy: 315, 55
  size: 28, 51
  orig: 28, 51
  offset: 0, 0
  index: 3
walk
  rotate: false
  xy: 141, 56
  size: 27, 52
  orig: 27, 52
  offset: 0, 0
  index: 4
walk
  rotate: false
  xy: 326, 2
  size: 28, 51
  orig: 28, 51
  offset: 0, 0
  index: 5
walk
  rotate: false
  xy: 537, 2
  size: 28, 50
  orig: 28, 50
  offset: 0, 0
  index: 6
walk
  rotate: false
  xy: 541, 54
  size: 28, 50
  orig: 28, 50
  offset: 0, 0
  index: 7
walk
  rotate: false
  xy: 345, 55
  size: 28, 51
  orig: 28, 51
  offset: 0, 0
  index: 8
walk
  rotate: false
  xy: 510, 54
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: 9
