package com.mat.way.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mat.way.Main;
import com.mat.way.MotionController;
import com.mat.way.Study;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class DesktopLauncher extends JPanel implements KeyListener{

	static Study study;
	static MotionController mController;

	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		study = new Study();
//		mController = new MotionController(new SamuraiJack());
//		study.setController(mController);
		new LwjglApplication(new Main(), config);
		System.out.print("test");
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.print("keycode: " + e.getKeyCode());
		switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				mController.leftPressed();
				break;
			case KeyEvent.VK_RIGHT:
				mController.rightPressed();
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				mController.leftReleased();
				break;
			case KeyEvent.VK_RIGHT:
				mController.rightReleased();
				break;
		}
	}

}
