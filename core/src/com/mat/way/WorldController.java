package com.mat.way;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mpodolsky on 09.09.2015.
 */
public class WorldController {

    enum Keys {
        LEFT, RIGHT, JUMP, ATTACK, CROUCH
    }

    private World   world;
    private Jack    jack;

    static Map<Keys, Boolean> keys = new HashMap<Keys, Boolean>();
    static {
        keys.put(Keys.LEFT, false);
        keys.put(Keys.RIGHT, false);
        keys.put(Keys.JUMP, false);
        keys.put(Keys.ATTACK, false);
        keys.put(Keys.CROUCH, false);
    }

    public WorldController(World world) {
        this.world = world;
        this.jack = world.getJack();
    }

            // ** Key presses and touches **************** //

    public void leftPressed() {
        keys.get(keys.put(Keys.LEFT, true));
    }

    public void rightPressed() {
        keys.get(keys.put(Keys.RIGHT, true));
    }

    public void jumpPressed() {
        keys.get(keys.put(Keys.JUMP, true));
    }

    public void firePressed() {
        keys.get(keys.put(Keys.ATTACK, true));
    }

    public void crouchPressed() {
        keys.get(keys.put(Keys.CROUCH, true));
    }

    public void leftReleased() {
        keys.get(keys.put(Keys.LEFT, false));
    }

    public void rightReleased() {
        keys.get(keys.put(Keys.RIGHT, false));
    }

    public void jumpReleased() {
        keys.get(keys.put(Keys.JUMP, false));
    }

    public void fireReleased() {
        keys.get(keys.put(Keys.ATTACK, false));
    }

    public void crouchReleased() {
        keys.get(keys.put(Keys.CROUCH, false));
    }

            /** The main update method **/
    public void update(float delta) {
        processInput();
        jack.update(delta);
    }

            /** Change Jack's state and parameters based on input controls **/
    private void processInput() {
        if (keys.get(Keys.LEFT)) {
            // left is pressed
            if (!keys.get(Keys.RIGHT)) {
                jack.setFacingLeft(true);
            }
            if (!keys.get(Keys.CROUCH)) {
                jack.setState(Jack.State.RUNNING);
                jack.getVelocity().x = -Jack.SPEED * 2f;
            } else {
                jack.setState(Jack.State.CROUCH_WALKING);
                jack.getVelocity().x = -Jack.SPEED;
            }
        }
        if (keys.get(Keys.RIGHT)) {
            // left is pressed
            if (!keys.get(Keys.LEFT)) {
                jack.setFacingLeft(false);
            }
            if (!keys.get(Keys.CROUCH)) {
                jack.setState(Jack.State.RUNNING);
                jack.getVelocity().x = Jack.SPEED * 2f;
            } else {
                jack.setState(Jack.State.CROUCH_WALKING);
                jack.getVelocity().x = Jack.SPEED;
            }
        }
        // need to check if both or none direction are pressed, then Bob is idle
        if ((keys.get(Keys.LEFT) && keys.get(Keys.RIGHT)) ||
        (!keys.get(Keys.LEFT) && !(keys.get(Keys.RIGHT)))) {
            if (jack.getState() == Jack.State.RUNNING) {
                jack.setState(Jack.State.IDLE);
//                if (jack.isFacingLeft()) {
//                    jack.getVelocity().x = -Jack.SPEED;
//                } else {
//                    jack.getVelocity().x = Jack.SPEED;
//                }
            }
//            jack.setState(Jack.State.IDLE);
            // acceleration is 0 on the x
            jack.getAcceleration().x = 0;
            // horizontal speed is 0
            jack.getVelocity().x = 0;
        }
    }
}

