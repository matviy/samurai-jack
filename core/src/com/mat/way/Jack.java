package com.mat.way;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by mpodolsky on 09.09.2015.
 */
public class Jack {

    public enum State {
        IDLE, WALKING, JUMPING, DYING, RUNNING, CROUCH_WALKING, STOPPING
    }

    static final float SPEED = 2f;  // unit per second
    static final float JUMP_VELOCITY = 1f;
    static final float SIZE = 2f; // multiple frame size

    Vector2 position = new Vector2();
    Vector2     acceleration = new Vector2();
    Vector2     velocity = new Vector2();
    Rectangle bounds = new Rectangle();
    State       state = State.IDLE;
    boolean     facingLeft = true;

    public Jack(Vector2 position) {
        this.position = position;
        this.bounds.height = SIZE;
        this.bounds.width = SIZE;
    }

    public Vector2 getPosition() {
        return position;
    }

    public static float getSPEED() {
        return SPEED;
    }

    public static float getJumpVelocity() {
        return JUMP_VELOCITY;
    }

    public static float getSIZE() {
        return SIZE;
    }

    public Vector2 getAcceleration() {
        return acceleration;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public State getState() {
        return state;
    }

    public boolean isFacingLeft() {
        return facingLeft;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setAcceleration(Vector2 acceleration) {
        this.acceleration = acceleration;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public void setBounds(Rectangle bounds) {
        this.bounds = bounds;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setFacingLeft(boolean facingLeft) {
        this.facingLeft = facingLeft;
    }

    public void update(float delta) {
        position.add(velocity.cpy().scl(delta));
    }

}
