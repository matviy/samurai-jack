package com.mat.way.constants;

public interface Constants {
    public static final int POSITION_NONE = -1;
    public static final int POSITION_LEFT = 0;
    public static final int POSITION_RIGHT = 1;
    public static final int POSITION_CROUCH = 2;

    static final float                      CAMERA_WIDTH = 10f;
    static final float                      CAMERA_HEIGHT = 7f;
}
