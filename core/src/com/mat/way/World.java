package com.mat.way;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mat.way.model.Button;

/**
 * Created by mpodolsky on 09.09.2015.
 */
public class World {

    Jack jack;

    Array<Button> buttons = new Array<Button>();


    public World() {
        createWord();
    }

    private void createWord() {
        jack = new Jack(new Vector2(5, 1));

        createButtons();
    }

    private void createButtons() {
        buttons.add(new Button(new Vector2(0.5f, 0.1f), new Vector2(1.5f, 1))); //left
        buttons.add(new Button(new Vector2(2f, 0.1f), new Vector2(1.5f, 1))); //right
        buttons.add(new Button(new Vector2(8.4f, 0.1f), new Vector2(1.5f, 1.5f))); //crouch
    }

    public Jack getJack() {
        return jack;
    }

    public Array<Button> getButtons() {
        return buttons;
    }
}
