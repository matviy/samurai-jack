package com.mat.way;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class SamuraiJack implements KeyListener {
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public enum State {
        IDLE, WALKING_LEFT, WALKING_RIGHT
    }

    static final float SPEED = 2f;  // unit per second
    static final float JUMP_VELOCITY = 1f;
    static final float SIZE = 0.5f; // half a unit

    State       state = State.IDLE;
    float       x, y;
    boolean     facingRight = true;

    public void update(State state, float x, float y, boolean facingRight) {
        this.state = state;
        this.x = x;
        this.y = y;
        this.facingRight = facingRight;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return this.state;
    }

}

