package com.mat.way;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.mat.way.constants.Constants;
import com.mat.way.model.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpodolsky on 09.09.2015.
 */
public class MainScreen implements Screen, InputProcessor {

    private World world;
    private WorldRenderer renderer;
    private WorldController controller;

    private float ppuY; // pixels per unit on the Y axis

    private int[] pointers = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    private List<Rectangle> rects = new ArrayList<Rectangle>();

    private int width, height;

    @Override
    public void show() {
        world = new World();
        renderer = new WorldRenderer(world);
        controller = new WorldController(world);
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
        controller.update(delta);
    }

    @Override
    public void resize(int w, int h) {
        renderer.setSize(w, h);
        width = w;
        height = h;
        ppuY = height / Constants.CAMERA_HEIGHT;
        initButtonRects();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.LEFT)
        controller.leftPressed();
        if (keycode == Input.Keys.RIGHT)
        controller.rightPressed();
        if (keycode == Input.Keys.Z)
        controller.jumpPressed();
        if (keycode == Input.Keys.X)
        controller.firePressed();
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.LEFT)
        controller.leftReleased();
        if (keycode == Input.Keys.RIGHT)
        controller.rightReleased();
        if (keycode == Input.Keys.Z)
        controller.jumpReleased();
        if (keycode == Input.Keys.X)
        controller.fireReleased();
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        System.out.println("tester down " + pointer + " pos: " + x + " " + y);
        checkTouchCollision(x, y, pointer);
        System.out.println("tester pointers[" + pointers[0] + "," + pointers[1] + "," + pointers[2] + "]");
        return true;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        System.out.println("tester up " + pointer + " pos: " + x + " " + y);
        touchReleased(pointer);
        System.out.println("tester pointers[" + pointers[0] + "," + pointers[1] + "," + pointers[2] + "]");
        return true;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        System.out.println("tester drag " + pointer + " pos: " + x + " " + y);
        checkTouchCollision(x, y, pointer);
        System.out.println("tester pointers[" + pointers[0] + "," + pointers[1] + "," + pointers[2] + "]");
        return false;
    }

    @Override
    public boolean mouseMoved(int x, int y) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        // TODO Auto-generated method stub
        return false;
    }

    private void checkTouchCollision(int x, int y, int pointer) {
        final int prevTouched = pointers[pointer];
        for (int i = rects.size() - 1; i >= 0; i--) {
            if (rects.get(i).contains(x,y)) {
                pointers[pointer] = i;
                break;
            }
            if (i == 0) {
                pointers[pointer] = Constants.POSITION_NONE;
            }
        }
        if (pointers[pointer] != prevTouched) {
            if (pointers[pointer] != Constants.POSITION_NONE) {
                switch (pointers[pointer]) {
                    case Constants.POSITION_LEFT:
                        controller.leftPressed();
                        break;
                    case Constants.POSITION_RIGHT:
                        controller.rightPressed();
                        break;
                    case Constants.POSITION_CROUCH:
                        controller.crouchPressed();
                        break;
                }
            }
            if (prevTouched != Constants.POSITION_NONE) {
                for (int i = pointers.length - 1; i >= 0; i--) {
                    if (pointers[i] == prevTouched) {
                        break;
                    }
                    if (i == 0) {
                        switch (prevTouched) {
                            case Constants.POSITION_LEFT:
                                controller.leftReleased();
                                break;
                            case Constants.POSITION_RIGHT:
                                controller.rightReleased();
                                break;
                            case Constants.POSITION_CROUCH:
                                controller.crouchReleased();
                                break;
                        }
                    }
                }
            }
        }
    }

    private void touchReleased(int pointer) {
        if (pointers[pointer] != Constants.POSITION_NONE) {
            for (int i = pointers.length - 1; i >= 0; i--) {
                if (pointers[i] == pointers[pointer] && i != pointer) {
                    break;
                }
                if (i == 0) {
                    switch (pointers[pointer]) {
                        case Constants.POSITION_LEFT:
                            controller.leftReleased();
                            break;
                        case Constants.POSITION_RIGHT:
                            controller.rightReleased();
                            break;
                        case Constants.POSITION_CROUCH:
                            controller.crouchReleased();
                            break;
                    }
                }
            }
            pointers[pointer] = Constants.POSITION_NONE;
        }
    }

    private void initButtonRects() {
        Array<Button> buttons = world.getButtons();
        rects.clear();
        for (Button button : buttons) {
            rects.add(new Rectangle(button.getPosition().x * ppuY, height - button.getPosition().y * ppuY - button.getSize().y * ppuY, button.getSize().x * ppuY, button.getSize().y * ppuY));
        }
//        rects.set(2, new Rectangle(width / 2, 0, width / 2, height));
    }

}
