package com.mat.way;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;


public class Study extends Game implements InputProcessor, ApplicationListener {
	private static final int        FRAME_COLS = 6;         // #1
	private static final int        FRAME_ROWS = 5;         // #2
	public static final int        FORWARD = 1;         // #2
	public static final int        BACK = 2;         // #2
	public static final int        STAY = 0;         // #2

	Animation walkAnimation;          // #3
	Animation backWalkAnimation;          // #3
	Texture                         walkSheet;              // #4
	Texture                         stay;              // #4
	TextureRegion[]                 walkFrames;             // #5
	TextureRegion[]                 backWalkFrames;             // #5
	SpriteBatch                     spriteBatch;            // #6
	Sprite                   currentFrame;           // #7
	TextureRegion                   stayTR;           // #7
	int currentFrame1 = 0;

	private TextureAtlas atlas = new TextureAtlas();
	private TextureRegion hero = new TextureRegion();
	private Array<TextureAtlas.AtlasRegion> sjList;
	private Array<Sprite> skeleton;
	Sprite Jack;
	TextureRegion heroRegion;
	Animation heroAnimation;
	Animation heroAnimatedSprite;

	float stateTime;                                        // #8
	int pos;
	int direction = FORWARD;
	private SamuraiJack.State mSJState = SamuraiJack.State.IDLE;
	private MotionController mController;
	float animationElapsed = 0f;
	float frameLength = 0.1f;
	
	@Override
	public void create () {
		atlas = new TextureAtlas("sj_sheet.txt");
		//set our hero TextureRegion to the hero sprite in the atlas
		heroRegion = atlas.findRegion("stay");
		sjList = atlas.findRegions("stay");
		skeleton = atlas.createSprites("stay");


		walkSheet = new Texture(Gdx.files.internal("sj_sheet.png")); // #9
		stay = new Texture(Gdx.files.internal("ic_tag.png")); // #9
		stayTR = new TextureRegion(stay, stay.getWidth(), stay.getHeight());
//		TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);              // #10
//		TextureRegion[][] tmp1 = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);              // #10
		walkFrames = new TextureRegion[skeleton.size];
		backWalkFrames = new TextureRegion[skeleton.size];
		TextureRegion[] tmp = new TextureRegion[skeleton.size];
		int index = 0;
		for (int i = 0; i < skeleton.size; i++) {
			Sprite tmpS = skeleton.get(i);
			walkFrames[i] = tmpS;
			backWalkFrames[i] = tmpS;
		}
//		for (int i = 0; i < FRAME_ROWS; i++) {
//			for (int j = 0; j < FRAME_COLS; j++) {
//				walkFrames[index] = tmp[i][j];
//				tmp1[i][j].flip(true, false);
//				backWalkFrames[index++] = tmp1[i][j];
//			}
//		}
		walkAnimation = new Animation(frameLength, sjList);      // #11
		backWalkAnimation = new Animation(frameLength, backWalkFrames);      // #11
		spriteBatch = new SpriteBatch();                // #12
		stateTime = 0f;                         // #13
	}

	@Override
	public void render () {
		if (mController != null) {
			mSJState = mController.getSJ().getState();
		}
		mSJState = SamuraiJack.State.WALKING_RIGHT;

		switch (mSJState) {
			case IDLE:
				direction = STAY;
				break;
			case WALKING_LEFT:
				direction = BACK;
//				pos = pos - 2;
				break;
			case WALKING_RIGHT:
				direction = FORWARD;
//				pos = pos + 2;
				break;
		}

//		float dt = Gdx.graphics.getDeltaTime();
//		animationElapsed += dt;
//		while(animationElapsed > frameLength){
//			animationElapsed -= frameLength;
//			currentFrame1 = (currentFrame1 == skeleton.size - 1) ? 0 : ++currentFrame1;
//		}
//		Jack = skeleton.get(currentFrame1);
//		Jack.setPosition(50+pos, 50);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);                        // #14
		stateTime += Gdx.graphics.getDeltaTime();           // #15
		if (direction == FORWARD) {
			currentFrame = (Sprite)walkAnimation.getKeyFrame(stateTime, true);  // #16
			currentFrame.setFlip(false, false);

//			if (currentFrame.isFlipX()) {
//				currentFrame.flip(true, false);
//			}
		} else if (direction == BACK) {
			currentFrame = (Sprite)walkAnimation.getKeyFrame(stateTime, true);
			currentFrame.setFlip(true, false);
//			currentFrame = (Sprite)backWalkAnimation.getKeyFrame(stateTime, true);  // #16
//			if (currentFrame.isFlipX()) {
//				currentFrame.flip(true, false);
//			}

		} else {
			currentFrame = (Sprite)stayTR;
		}
		spriteBatch.begin();
//		Jack.draw(spriteBatch);
		spriteBatch.draw(currentFrame, 50 + pos, 50);             // #17
		spriteBatch.end();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public int getDirection() {
		return direction;
	}

	public void setController(MotionController c) {
		mController = c;
	}

	public void setKeyListener() {
	}
}
