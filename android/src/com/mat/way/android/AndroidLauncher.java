package com.mat.way.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mat.way.Main;
import com.mat.way.MotionController;
import com.mat.way.Study;

public class AndroidLauncher extends AndroidApplication {

	private Study study;
	private MotionController mController;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		study = new Study();
//		mController = new MotionController(new SamuraiJack());
//		study.setController(mController);
//		initialize(study, config);
		initialize(new Main(), config);

	}

}
