package com.mat.way;

import java.util.HashMap;
import java.util.Map;

public class MotionController {
    enum Keys {
        LEFT, RIGHT, JUMP, FIRE
    }
    private SamuraiJack mSamuraiJack;
    private int direction;

    static Map<Keys, Boolean> keys = new HashMap<Keys, Boolean>();
    static {
        keys.put(Keys.LEFT, false);
        keys.put(Keys.RIGHT, false);
        keys.put(Keys.JUMP, false);
        keys.put(Keys.FIRE, false);
    };

    public MotionController(SamuraiJack samuraiJack) {
        this.mSamuraiJack = samuraiJack;
    }

            // ** Key presses and touches **************** //

    public void leftPressed() {
        mSamuraiJack.setState(SamuraiJack.State.WALKING_LEFT);
    }

    public void rightPressed() {
        mSamuraiJack.setState(SamuraiJack.State.WALKING_RIGHT);
    }

    public void jumpPressed() {
        keys.get(keys.put(Keys.JUMP, true));
    }

    public void firePressed() {
        keys.get(keys.put(Keys.FIRE, false));
    }

    public void leftReleased() {
        mSamuraiJack.setState(SamuraiJack.State.IDLE);
    }

    public void rightReleased() {
        mSamuraiJack.setState(SamuraiJack.State.IDLE);
    }

    public void jumpReleased() {
        keys.get(keys.put(Keys.JUMP, false));
    }

    public void fireReleased() {
        keys.get(keys.put(Keys.FIRE, false));
    }

            /** The main update method **/
    public void update(float delta) {
//        samuraiJack.update(delta);
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public SamuraiJack getSJ() {
        return mSamuraiJack;
    }

            /** Change SamuraiJack's state and parameters based on input controls **/
}

