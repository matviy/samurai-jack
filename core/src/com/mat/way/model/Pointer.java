package com.mat.way.model;

/**
 * Created by mpodolsky on 30.09.2015.
 */
public class Pointer {

    public enum Position {
        NONE, LEFT, RIGHT
    }

    private float x;
    private float y;
    private Position pos;

    public Pointer(float x, float y, Position pos) {
        this.x = x;
        this.y = y;
        this.pos = pos;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        this.pos = pos;
    }
}
