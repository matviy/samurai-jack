package com.mat.way.model;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by mpodolsky on 01.10.2015.
 */
public class Button {

    Vector2 position = new Vector2();
    Vector2 size = new Vector2();

    public Button(Vector2 position, Vector2 size) {
        this.position = position;
        this.size = size;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Vector2 getSize() {
        return size;
    }

    public void setSize(Vector2 size) {
        this.size = size;
    }
}
