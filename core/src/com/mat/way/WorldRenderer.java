package com.mat.way;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.mat.way.constants.Constants;
import com.mat.way.model.Button;

/**
 * Created by mpodolsky on 09.09.2015.
 */
public class WorldRenderer {

    public static final float                       FRAME_DURATION = 0.1f;


    private World                                   world;
    private OrthographicCamera                      cam;
    private SpriteBatch                             spriteBatch;
    private float                                   stateTime;

    private Texture                                 jackTexture;
    private Texture                                 jackTextureSheet;
    private Sprite                                  jackSprite;
    private TextureAtlas                            atlas = new TextureAtlas();
    private TextureRegion                           currentFrame;

    //Sprites
    private Array<TextureAtlas.AtlasRegion>         jackStayFrames;
    private Array<TextureAtlas.AtlasRegion>         jackWalkFrames;
    private Array<TextureAtlas.AtlasRegion>         jackRunFrames;
    private Array<TextureAtlas.AtlasRegion>         jackCrouchWalkFrames;
    private Array<TextureAtlas.AtlasRegion>         jackStoppingFrames;

    //animations
    private Animation                               jackWalkAnim;
    private Animation                               jackRunAnim;
    private Animation                               jackStayAnim;
    private Animation                               jackCrouchWalkAnim;
    private Animation                               jackStoppingAnim;

    private float                                     width;
    private float                                     height;
    private float                                   ppuX; // pixels per unit on the X axis
    private float                                   ppuY; // pixels per unit on the Y axis

    private Jack                                    jack;
    private float                                   frameWidth, frameHeight;
    private Jack.State                              currentState = Jack.State.IDLE;
    private Jack.State                              newState;

    //buttons
    private Array<Texture>                          buttonTextures;
    private Array<Button>                           buttons;
    private Button                                  button;


    public void setSize (int w, int h) {
        this.width = (float)w;
        this.height = (float)h;
        ppuX = width / Constants.CAMERA_WIDTH;
        ppuY = height / Constants.CAMERA_HEIGHT;
    }


    ShapeRenderer debugRenderer = new ShapeRenderer();

    public WorldRenderer(World world) {
        this.world = world;
        this.cam = new OrthographicCamera(10, 7);
        this.cam.position.set(5f, 3.5f, 0);
        this.cam.update();
        spriteBatch = new SpriteBatch();
        stateTime = 0f;
        jack = world.getJack();
        buttons = world.getButtons();
        buttonTextures = new Array<Texture>();
        loadTextures();
    }

    private void loadTextures() {
        jackTexture = new Texture(Gdx.files.internal("ic_tag.png"));
        jackSprite = new Sprite(jackTexture);
        buttonTextures.add(new Texture(Gdx.files.internal("leftleft.png")));
        buttonTextures.add(new Texture(Gdx.files.internal("rightright.png")));
        buttonTextures.add(new Texture(Gdx.files.internal("down.png")));

        atlas = new TextureAtlas("sj_sheet.txt");
        jackStayFrames = atlas.findRegions("stay");
        jackWalkFrames = atlas.findRegions("walk");
        jackRunFrames = atlas.findRegions("run");
        jackCrouchWalkFrames = atlas.findRegions("crouch_walk");
        jackStoppingFrames = atlas.findRegions("slide_stop");
        jackStayAnim = new Animation(FRAME_DURATION, jackStayFrames);
        jackWalkAnim = new Animation(FRAME_DURATION, jackWalkFrames);
        jackRunAnim = new Animation(FRAME_DURATION, jackRunFrames);
        jackCrouchWalkAnim = new Animation(FRAME_DURATION, jackCrouchWalkFrames);
        jackStoppingAnim = new Animation(FRAME_DURATION, jackStoppingFrames);
    }


    public void render() {
        stateTime += Gdx.graphics.getDeltaTime();
        spriteBatch.begin();
        drawJack();
        drawButtons();
        spriteBatch.end();
//        drawAll();
    }

    private void drawJack() {
        currentFrame = getJackFrame(jack);
        frameWidth = currentFrame.getRegionWidth();
        frameHeight = currentFrame.getRegionHeight();
        if (jack.isFacingLeft() && currentFrame.isFlipX()) {
            currentFrame.flip(true, false);
        } else if (!jack.isFacingLeft() && !currentFrame.isFlipX()) {
            currentFrame.flip(true, false);
        }
//        spriteBatch.draw(currentFrame, jack.getPosition().x * ppuX, jack.getPosition().y * ppuY, Jack.SIZE * ppuX, Jack.SIZE * ppuY);
        spriteBatch.draw(currentFrame, jack.getPosition().x * ppuY, jack.getPosition().y * ppuY, frameWidth * Jack.SIZE, frameHeight * Jack.SIZE);
    }

    private void drawButtons() {
        int i = 0;
        for (Button button : world.getButtons()) {
            spriteBatch.draw(buttonTextures.get(i), button.getPosition().x * ppuY, button.getPosition().y * ppuY, button.getSize().x * ppuY, button.getSize().y * ppuY);
            i++;
        }

    }

    private void drawAll() {
        Jack jack = world.getJack();
        debugRenderer.setProjectionMatrix(cam.combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);

        Rectangle rect = jack.getBounds();
        float x1 = jack.getPosition().x + rect.x;
        float y1 = jack.getPosition().y + rect.y;
        debugRenderer.setColor(new Color(0, 1, 0, 1));
        debugRenderer.rect(x1, y1, rect.width, rect.height);
        debugRenderer.end();
    }

    private TextureRegion getJackFrame(Jack jack) {
        Jack.State state = jack.getState();
        if (state != currentState) {
            currentState = state;
            stateTime = 0f;
        }
        switch (state) {
            case IDLE:
                return jackStayAnim.getKeyFrame(stateTime, true);
            case WALKING:
                return jackWalkAnim.getKeyFrame(stateTime, true);
            case RUNNING:
                return jackRunAnim.getKeyFrame(stateTime, true);
            case CROUCH_WALKING:
                return jackCrouchWalkAnim.getKeyFrame(stateTime, true);
            case STOPPING:
                if (jackStoppingAnim.isAnimationFinished(stateTime)) {
                    jack.setState(Jack.State.IDLE);
                    jack.getVelocity().x = 0;
                }
                return jackStoppingAnim.getKeyFrame(stateTime, true);
            default:
                return jackStayAnim.getKeyFrame(stateTime, true);
        }
    }

}
