package com.mat.way;

import com.badlogic.gdx.Game;

/**
 * Created by mpodolsky on 09.09.2015.
 */
public class Main extends Game {
    @Override
    public void create() {
        setScreen(new MainScreen());
    }
}
